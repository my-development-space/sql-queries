SELECT 
	department_name,
	UPPER(department_name),
	INITCAP(department_name),
	LTRIM(department_name) as left_trim_dep_name,
	RTRIM(LTRIM(department_name)) as left_right_trimmed_dep_name,
	department_name || ' ' || division_name,
	department_name || ' ' || NULL as this_will_yeild_NULL,
	CONCAT(department_name, ' ', NULL) as this_wont_yeild_NULL,
	CONCAT_WS(' - ', id, department_name, division_name) as add_sep_between_every_field,
    SUBSTRING(department_name FROM 1 FOR 4) as first_4_letters,
	SUBSTRING(department_name FROM 5) as all_letters_after_5th_position
	
FROM
	data_sci.company_departments
/*WHERE
	division_name like '%domestic%'
OR
	division_name like '%auto%'
*/	
/*
Below WHERE clause is same as the above, but much efficient in compute (allowing regex)
*/
WHERE
	-- division_name SIMILAR TO '%domestic%|%auto%' 
	-- This is not same as division_name SIMILAR TO '%domestic% | %auto%'
	division_name SIMILAR TO '(auto|out)%|&%' -- matches division names starting with either auto OR out followed by any character and '&' and then again anything.