create extension if not exists fuzzystrmatch;

SELECT
	soundex('Postgres'), --P232
	soundex('Postgresss'), --P232
	('Postgres' = 'Postgresss'), --false
	soundex('Postgres') = soundex('Postgresss'), --true
	soundex('Kostgres'), --K232
	difference('Postgres' , 'Postgresss'), --4
	difference('Postgres' , 'Kostgres'), --3
	levenshtein('Postgres' , 'Postgres'), --0
	levenshtein('Postgres' , 'Kostgres'), --1
	levenshtein('Postgres' , 'mySQL') --8