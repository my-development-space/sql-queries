SELECT 
	department_id, SUM(salary), 
	MIN(salary), 
	MAX(salary), 
	round(AVG(salary),3) as average_salary, 
	round(VAR_POP(salary),3) as salary_variance,
	round(stddev_pop(salary),3) as salary_std
FROM 
	data_sci.employees
GROUP BY department_id