SELECT 
	emp.last_name,
	emp.email,
	emp.start_date,
	emp.salary,
	emp.job_title,
	cr.region_name, 
	cr.country_name
FROM
	data_sci.employees AS emp
JOIN
	data_sci.company_regions AS cr
ON
	emp.region_id = cr.id
WHERE
	cr.country_name = 'canada'